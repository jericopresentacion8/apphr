<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/alertify/css/alertify.css">
    <style type="text/css">
      input{
        text-transform:uppercase;
      }
      body{
          font-family: "Segoe UI";
      }
      #signa:hover{
        color: dodgerblue;
      }
    </style>
    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
</head>
<body style="background-color: #cccccc">

<div class="container-fluid">


  <div class="row" style="margin-top: 50px;margin-bottom: 50px">
      <div class="col-md-2">
        
      </div>
      <div class="col-md-8">

            <div class="col-md-12" style="background-color: dodgerblue;color: white">
                <h1 style="text-align: center">APPLICATION FOR EMPLOYMENT</h1>
                
            </div>
            <div class="col-md-12" style="background-color: whitesmoke">
                <hr style="border: 1px solid gray">
                <p style="font-size: 10.5px">THANKYOU FOR YOUR INTEREST IN OUR COMPANY. TO ENABLE US TO CONSIDER AND PROPERLY EVALUATE YOUR APPLICATION, PLEASE FILL-UP COMPLETELY ALL SECTION OF THIS. PRE APPLICATION FAILURE TO ANSWER COMPLETELY MAY RESULT IN THE REJECTION OR DELAY OF YOUR APPLICATION.</p>
            </div>
            <div class="col-md-12" style="background-color: whitesmoke;margin-bottom: 20px">

                  <table class="table" style="width: 100%;border: 2px solid gray">
                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>EMPLOYMENT CATEGORY</label></td>
                    </tr>
                    <tr>
                      <td colspan="2"><label>Position Applied For</label><input class="form-control" autocomplete="off" id="position_applied" type="text" name="position_applied"></td>
                      <td colspan="2"><label>Alternative Position</label><input class="form-control" id="alternative_position" type="text" name="alternative_position"></td>
                    </tr>

                    <tr>
                      <td colspan="2"><label>Salary Expectation</label><input class="form-control" id="salary_expectation" type="text" name="salary_expectation"></td>
                      <td colspan="2"><label>Date Available For Work</label><input class="form-control" id="date_available" type="date" name="date_available"></td>
                    </tr>

                    <tr>
                      <td colspan="2"><label>Referred By</label><input class="form-control" id="referredby" type="text" name="referredby"></td>
                      <td colspan="2"><label>Date Today</label><input disabled="" value="<?php echo date('Y-m-d') ?>" class="form-control" id="datetoday" type="date" name="datetoday"></td>
                    </tr>
   
                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>PERSONAL INFORMATION</label></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Name (Lastname)</label><input class="form-control" type="text" id="lastname" name="lastname"></td>
                      <td style="width: 25%"><label>Firstname</label><input class="form-control" type="text" id="firstname" name="firstname"></td>
                      <td style="width: 25%"><label>Middlename</label><input class="form-control" type="text" id="middlename" name="middlename"></td>
                      <td style="width: 25%"><label>Nicknames</label><input class="form-control" type="text" id="nickname" name="nickname"></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Date of Birth</label><input class="form-control" id="dateofbirth" type="date" name="dateofbirth"></td>
                      <td colspan="2"><label>Place of Birth</label><input class="form-control" id="placeofbirth" type="text" name="placeofbirth"></td>
                      <td style="width: 25%"><label>Age</label><input class="form-control" id="age" type="number" name="age"></td>
                    </tr>
                    <tr>
                      <td colspan="2"><label>Nationality</label><input class="form-control" type="text" id="nationality" name="nationality"></td>
                      <td ><label>Civil Status</label><select class="form-control" id="civil_status" name="civil_status">
                        <option value="MARRIED" >MARRIED</option>
                        <option value="WIDOWED" >WIDOWED</option>
                        <option value="SEPARATED" >SEPARATED</option>
                        <option value="DIVORCED" >DIVORCED</option>
                        <option value="SINGLE" >SINGLE</option>
                      </select></td>
                      <!-- <input class="form-control" type="text" id="civil_status" name="civil_status"> -->
                      <td ><label>Sex</label><select  class="form-control" id="sex" name="sex">
                        <option value="MALE">MALE</option>
                        <option value="FEMALE">FEMALE</option>
                      </select></td>  
                      <!-- <input class="form-control" type="text" id="sex" name="sex">         -->
                    </tr>
                    <tr>
                      <td colspan="2"><label>Religion</label><input class="form-control" type="text" id="religion" name="religion"></td>
                      <td><label>Height (cm)</label><input class="form-control" type="text" id="height" name="height"></td>
                      <td><label>Weight (kg)</label><input class="form-control" type="text" id="weight" name="weight"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Present Address(NO/STREET/CITY/TOWN)</label><input class="form-control" type="text" id="address" name="address"></td>
                      <td><label>Lenght Of Stay</label><input class="form-control" type="text" id="lengthlive" name="lengthlive"></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Telephone No</label><input class="form-control" type="text" placeholder="" id="telephone" name="telephone"></td>
                      <td style="width: 25%"><label>Cellular No</label><input class="form-control" type="text" placeholder="" id="cellular" name="cellular"></td>
                      <td style="width: 25%"><label>Contact Person</label><input class="form-control" type="text" placeholder="" id="contact_person" name="contact_person"></td>
                      <td style="width: 25%"><label>Available Time</label><input class="form-control" type="text" placeholder="" id="available_time" name="available_time"></td>
                    </tr>


                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>EDUCATION BACKGROUND</label></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>College</label><input class="form-control" type="text" id="ebcollege1" name="ebcollege1"></td>
                      <td><label>Date Attended</label><input class="form-control" type="date" id="ebdateattended1" name="ebdateattended1"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Address</label><input class="form-control" type="text" id="ebaddress1" name="ebaddress1"></td>
                      <td><label>Course</label><input class="form-control" type="text" id="ebcourse1" name="ebcourse1"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Post Graduate/Trade/Technical Training</label><input class="form-control" type="text" id="ebcollege2" name="ebcollege2"></td>
                      <td><label>Date Attended</label><input class="form-control" type="date" id="ebdateattended2" name="ebdateattended2"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Address</label><input class="form-control" type="text" id="ebaddress2" name="ebaddress2"></td>
                      <td><label>Course</label><input class="form-control" type="text" id="ebcourse2" name="ebcourse2"></td>
                    </tr>

                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>EMPLOYMENT HISTORY</label></td>
                    </tr>
                    <tr>
                      <td colspan="4"><p><i>Begin with the most recent employer (attached additional sheet if needed)</i></p></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Position</label><input class="form-control" type="text" id="ehposition1" name="ehposition1"></td>
                      <td colspan="2"><label>Employer</label><input class="form-control" type="text" id="ehemployer1" name="ehemployer1"></td>
                      <td style="width: 25%"><label>Date of Employment</label><input class="form-control" type="date" id="ehdoe1" name="ehdoe1"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Employer's Address</label><input class="form-control" type="text" id="ehaddress1" name="ehaddress1"></td>
                      <td><label>Phone No</label><input class="form-control" type="text" id="ehphoneno1" name="ehphoneno1"></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Position</label><input class="form-control" type="text" id="ehposition2" name="ehposition2"></td>
                      <td colspan="2"><label>Employer</label><input class="form-control" type="text" id="ehemployer2" name="ehemployer2"></td>
                      <td style="width: 25%"><label>Date of Employment</label><input class="form-control" type="date" id="ehdoe2" name="ehdoe2"></td>
                    </tr>

                    <tr>
                      <td colspan="3"><label>Employer's Address</label><input class="form-control" type="text" id="ehaddress2" name="ehaddress2"></td>
                      <td><label>Phone No</label><input class="form-control" type="text" id="ehphoneno2" name="ehphoneno2"></td>
                    </tr>

                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>CHARACTER REFERENCES</label></td>
                    </tr>

                    <tr>
                      <td colspan="4"><p><i>LIST AT LEAST THREE (3) PROFESSIONAL REFERENCES WHO ARE FAMILIAR WITH THE QUALITY OF YOUR WORK HAVE WORKED DIRECTLY WITH YOU AND HAVE KNOWN YOU FOR AT LEAST TWO (2) YEARS.(DO NOT INCLUDE RELATIVES)</i></p></td>
                    </tr>

                    <tr>
                      <td><label>Name</label><input class="form-control" type="text" id="crname1" name="crname1"></td>
                      <td colspan="3"><label>Adress</label><input class="form-control" type="text" id="craddress1" name="craddress1"></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Occupation</label><input class="form-control" type="text" id="croccupation1" name="croccupation1"></td>
                      <td colspan="2"><label>Phone Nos.</label><input class="form-control" type="text" id="crphoneno1" name="crphoneno1"></td>
                      <td style="width: 25%"><label>Relationship</label><input class="form-control" type="text" id="crrelationship1" name="crrelationship1"></td>
                    </tr>

                    <tr>
                      <td><label>Name</label><input class="form-control" type="text" id="crname2" name="crname2"></td>
                      <td colspan="3"><label>Adress</label><input class="form-control" type="text" id="craddress2" name="craddress2"></td>
                    </tr>

                    <tr>
                      <td style="width: 25%"><label>Occupation</label><input class="form-control" type="text" id="croccupation2" name="croccupation2"></td>
                      <td colspan="2"><label>Phone Nos.</label><input class="form-control" type="text" id="crphoneno2" name="crphoneno2"></td>
                      <td style="width: 25%"><label>Relationship</label><input class="form-control" type="text" id="crrelationship2" name="crrelationship2"></td>
                    </tr>

                    <tr style="background-color: dodgerblue;color: white">
                      <td colspan="4" style="text-align: center"><label>DECLARATION</label></td>
                    </tr>

                    <tr>
                      <td colspan="4"><p><i>I hereby declare that my answers to the questions on this application are true and correct and I give the right to the company to Investigate, ascertain and or verify all Information, If necessary. I hereby release all liability or responsibility from all persons, companies or corporations furnishing such information.</i></p></td>
                    </tr>

                    <tr style="text-align: center">
                      <td colspan="3" style="text-align: right;"></td>
                      <td><label style="cursor: pointer;" id="signa" onclick="signature()">Signature of Applicant</label><br><span style="font-size: 11px;text-align: center">(click the Signature of Applicant)</span></td>
                    </tr>

                  </table>

           
            </div>

            <center>
              <input type="text" id="stat" hidden="" disabled="" value="notsave" name="">
            <button class="btn btn-success" type="button" onclick="saveurl()"><b>Save And View</b></button>
            <button class="btn btn-primary" type="button" onclick="sendemail()"><b>Send To Email</b></button>
            <button class="btn btn-danger" type="button" onclick="refresh()"><b>New</b></button>
            </center>

      </div>
      <div class="col-md-2">
        
      </div>

      
      
  </div>



    
</div>



  <!-- Modal -->
  <div class="modal fade" id="signamodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style="background-color: dodgerblue;color: white">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center">Write your Signature</h4>
        </div>
        <div class="modal-body">
            <center>
            <canvas id="myCanvas" style="border:1px solid lightgray" width="500" height="200"></canvas>
            <br>
            <button class="btn btn-success" type="button" onclick="pdf()" name="generatepdf"><b>Done Signature</b></button>
            <button class="btn btn-primary" onclick="clearArea()"><b>Clear</b></button>
            <button type="button" onclick="smtp()" name="sendmail" style="display: none;">open</button>

            <button type="button" onclick="img()" style="display: none;">img</button>
            <img src="" id="canvaspic" style="border: 1px solid black;width: 100px;height: 100px;display: none;">
            <textarea id="urls" style="display: none;"></textarea>
            <input type="text" id="urlid" name="" style="display: none;">
            
            </center>
        </div>
        <div class="modal-footer" style="background-color: white;color: white">
        </div>
      </div>
    </div>
  </div>


</body>
<script src="vendor/jquery/jquery-3.2.1.min.js"></script> 
<script src="tables/js/jquery.dataTables.min.js"></script> 
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/alertify/alertify.js"></script>
<script src="tables/js/dataTables.bootstrap4.min.js"></script>
<script>

  function refresh(){
    window.location.href="index.php";
  }
  function signature(){
    $('#signamodal').modal({backdrop: 'static', keyboard: false});
  }

  // $('#catmodal').modal({backdrop: 'static', keyboard: false});

  function pdf(){

    var b = $('#myCanvas');
    const dataURL = b[0].toDataURL();
    var aa = dataURL.toString();
    var res = aa.split(",");
    var dd = res[1].toString();
    // alert(dd);
    $('#urls').val(dd);
    $('#signamodal').modal('hide');
    // window.open("tcpdf/examples/generatepdf.php?dataURL="+dd);
  }

  function saveurl(){
    var urls = $('#urls').val();
    var position_applied = $('#position_applied').val();
    var alternative_position = $('#alternative_position').val();
    var salary_expectation = $('#salary_expectation').val();
    var date_available = $('#date_available').val();
    var referredby = $('#referredby').val();
    var datetoday = $('#datetoday').val();
    var lastname = $('#lastname').val();
    var firstname = $('#firstname').val();
    var middlename = $('#middlename').val();
    var nickname = $('#nickname').val();
    var dateofbirth = $('#dateofbirth').val();
    var placeofbirth = $('#placeofbirth').val();
    var age = $('#age').val();
    var nationality = $('#nationality').val();
    var civil_status = $('#civil_status').val();
    var sex = $('#sex').val();
    var religion = $('#religion').val();
    var height = $('#height').val();
    var weight = $('#weight').val();
    var address = $('#address').val();
    var lengthlive = $('#lengthlive').val();
    var telephone = $('#telephone').val();
    var cellular = $('#cellular').val();
    var contact_person = $('#contact_person').val();
    var available_time = $('#available_time').val();
    var ebcollege1 = $('#ebcollege1').val();
    var ebdateattended1 = $('#ebdateattended1').val();
    var ebaddress1 = $('#ebaddress1').val();
    var ebcourse1 = $('#ebcourse1').val();
    var ebcollege2 = $('#ebcollege2').val();
    var ebdateattended2 = $('#ebdateattended2').val();
    var ebaddress2 = $('#ebaddress2').val();
    var ebcourse2 = $('#ebcourse2').val();
    var ehposition1 = $('#ehposition1').val();
    var ehemployer1 = $('#ehemployer1').val();
    var ehdoe1 = $('#ehdoe1').val();
    var ehaddress1 = $('#ehaddress1').val();
    var ehphoneno1 = $('#ehphoneno1').val();
    var ehposition2 = $('#ehposition2').val();
    var ehemployer2 = $('#ehemployer2').val();
    var ehdoe2 = $('#ehdoe2').val();
    var ehaddress2 = $('#ehaddress2').val();
    var ehphoneno2 = $('#ehphoneno2').val();
    var crname1 = $('#crname1').val();
    var craddress1 = $('#craddress1').val();
    var croccupation1 = $('#croccupation1').val();
    var crphoneno1 = $('#crphoneno1').val();
    var crrelationship1 = $('#crrelationship1').val();
    var crname2 = $('#crname2').val();
    var craddress2 = $('#craddress2').val();
    var croccupation2 = $('#croccupation2').val();
    var crphoneno2 = $('#crphoneno2').val();
    var crrelationship2 = $('#crrelationship2').val();

    var dd = 7;
    $.ajax({
      url:"samplecontroller.php?addurl",
      method:"POST",
      data:{
        urls : urls,
        position_applied : position_applied,
        alternative_position : alternative_position,
        salary_expectation : salary_expectation,
        date_available : date_available,
        referredby : referredby,
        datetoday : datetoday,
        lastname : lastname,
        firstname : firstname,
        middlename : middlename,
        nickname : nickname,
        dateofbirth : dateofbirth,
        placeofbirth : placeofbirth,
        age : age,
        nationality : nationality,
        civil_status : civil_status,
        sex : sex,
        religion : religion,
        height : height,
        weight : weight,
        address : address,
        lengthlive : lengthlive,
        telephone : telephone,
        cellular : cellular,
        contact_person : contact_person,
        available_time : available_time,
        ebcollege1 : ebcollege1,
        ebdateattended1 : ebdateattended1,
        ebaddress1 : ebaddress1,
        ebcourse1 : ebcourse1,
        ebcollege2 : ebcollege2,
        ebdateattended2 : ebdateattended2,
        ebaddress2 : ebaddress2,
        ebcourse2 : ebcourse2,
        ehposition1 : ehposition1,
        ehemployer1 : ehemployer1,
        ehdoe1 : ehdoe1,
        ehaddress1 : ehaddress1,
        ehphoneno1 : ehphoneno1,
        ehposition2 : ehposition2,
        ehemployer2 : ehemployer2,
        ehdoe2 : ehdoe2,
        ehaddress2 : ehaddress2,
        ehphoneno2 : ehphoneno2,
        crname1 : crname1,
        craddress1 : craddress1,
        croccupation1 : croccupation1,
        crphoneno1 : crphoneno1,
        crrelationship1 : crrelationship1,
        crname2 : crname2,
        craddress2 : craddress2,
        croccupation2 : croccupation2,
        crphoneno2 : crphoneno2,
        crrelationship2 : crrelationship2

      },success:function(data){
        var b = $.parseJSON(data);
        var idnya = b.idnya;
        $('#urlid').val(idnya);
        alertify.success("Successfully saved");
        clearArea();
        window.open("tcpdf/examples/generateviewpdf.php?dataURL="+idnya);
      }
    });

  }

  function sendemail(){
    var idnya = $('#urlid').val();
    window.open("tcpdf/examples/generatepdf.php?dataURL="+idnya);
  }




  function smtp(){
    var a = "";
    $.ajax({
      url:"controllersetup.php?smtp",
      method:"POST",
      data:{
        a:a,
      },success:function(){
        alert("send");
      }
    });
  }

    // var canvas;
    // var ctx;
   


  


    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1.5;
    ctx.lineJoin = "round";


  canvas.addEventListener("touchstart", function (e) {
        mousePos = getTouchPos(canvas, e);
        var touch = e.touches[0];
        var mouseEvent = new MouseEvent("mousedown", {
          clientX: touch.clientX,
          clientY: touch.clientY
        });
          canvas.dispatchEvent(mouseEvent);
  }, false);

  canvas.addEventListener("touchend", function (e) {
        var mouseEvent = new MouseEvent("mouseup", {});
        canvas.dispatchEvent(mouseEvent);
  }, false);
  canvas.addEventListener("touchmove", function (e) {
        var touch = e.touches[0];
        var mouseEvent = new MouseEvent("mousemove", {
          clientX: touch.clientX,
          clientY: touch.clientY
        });
          canvas.dispatchEvent(mouseEvent);
  }, false);

  // Get the position of a touch relative to the canvas
  function getTouchPos(canvasDom, touchEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
      x: touchEvent.touches[0].clientX - rect.left,
      y: touchEvent.touches[0].clientY - rect.top
    };
  }


    // Prevent scrolling when touching the canvas
  document.body.addEventListener("touchstart", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchend", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchmove", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);

  var drawing = false;
  var mousePos = { x:0, y:0 };
  var lastPos = mousePos;
  canvas.addEventListener("mousedown", function (e) {
          drawing = true;
    lastPos = getMousePos(canvas, e);
  }, false);
  canvas.addEventListener("mouseup", function (e) {
    drawing = false;
  }, false);
  canvas.addEventListener("mousemove", function (e) {
    mousePos = getMousePos(canvas, e);
  }, false);

  // Get the position of the mouse relative to the canvas
  function getMousePos(canvasDom, mouseEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
      x: mouseEvent.clientX - rect.left,
      y: mouseEvent.clientY - rect.top
    };
  }


  window.requestAnimFrame = (function (callback) {
          return window.requestAnimationFrame || 
             window.webkitRequestAnimationFrame ||
             window.mozRequestAnimationFrame ||
             window.oRequestAnimationFrame ||
             window.msRequestAnimaitonFrame ||
             function (callback) {
          window.setTimeout(callback, 1000/60);
             };
  })();

  // Draw to the canvas
  function renderCanvas() {
    if (drawing) {
      ctx.beginPath();
      ctx.moveTo(lastPos.x, lastPos.y);
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
      ctx.closePath();
      ctx.stroke();
      lastPos = mousePos;
    }
  }

  // Allow for animation
  (function drawLoop () {
    requestAnimFrame(drawLoop);
    renderCanvas();
  })();




function clearArea() {
      // Use the identity matrix while clearing the canvas
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  }

  function img(){
    var b = $('#myCanvas');
    const dataURL = b[0].toDataURL();
    $('#canvaspic').attr('src', dataURL);
  }

</script>   
</html>


